#!/bin/sh

set -xeuo pipefail

IMAGE="visionfive-$(date +%Y%m%d).img"
MNTPNT="$(mktemp -d /tmp/starfive.XXXXXX)"
LOOP_DEVICE="$(losetup -f)"

if [ "$(id -u)" -ne 0 ]; then
	echo "Script needs to be run as root" 1>&2
	exit 1
fi

##
# Create image file.
##

cleanup() {
	set +e
	sync
	umount "$MNTPNT/boot" "$MNTPNT"
	losetup -d "${LOOP_DEVICE}"
	rm -rf "$MNTPNT"
}

trap cleanup INT EXIT

rm -f "$IMAGE"
truncate -s 170M "$IMAGE"

# XXX: The off-the-shelf firmware u-boot-jh7110 will search for boot partition
# that has either type=uefi (ESP C12A7328-F81F-11D2-BA4B-00A0C93EC93B) or has
# legacy boot attribute.
#
# Older u-boot will have this hardcoded reads the uEnv.txt from partition 3.

sfdisk "${IMAGE}" <<EOF
label: gpt
1: start=4096, size=4096, type=2E54B353-1271-4842-806F-E436D6AF6985
2: start=8192, size=8192, type=BC13C2FF-59E6-4262-A352-B275FD6F7172
3: start=16384,size=64M, type=linux, attrs=LegacyBIOSBootable
4: start=,     size=,     type=linux
EOF

losetup -P "${LOOP_DEVICE}" "${IMAGE}"

for partition in 3 4; do
	if [ ! -b "${LOOP_DEVICE}p${partition}" ]; then
		echo "Partition '${LOOP_DEVICE}p${partition}' does not exist" 1>&2
		exit 1
	fi
done

##
# Mount
##

mkfs.ext4 -L root "${LOOP_DEVICE}p4"
mount -t ext4 "${LOOP_DEVICE}p4" "$MNTPNT"

mkdir -p "$MNTPNT/boot"
mkfs.ext4 -L boot "${LOOP_DEVICE}p3"
mount -t ext4 "${LOOP_DEVICE}p3" "$MNTPNT/boot"

##
# Bootloader Configuration
##

bootuuid=$(blkid -s UUID -o value ${LOOP_DEVICE}p3)
rootuuid=$(blkid -s UUID -o value ${LOOP_DEVICE}p4)

mkdir -p "$MNTPNT"/boot/extlinux
cat <<EOF > "$MNTPNT"/boot/extlinux/extlinux.conf
menu title StarFive VisionFive
timeout 20
default alpineV13b

label alpineV13b
	menu label Alpine Linux visionfive V2-v1.3b
	kernel /vmlinuz-starfive
	initrd /initramfs-starfive
	fdt /dtbs-starfive/starfive/jh7110-starfive-visionfive-2-v1.3b.dtb
	append earlycon=sbi rw root=UUID=$rootuuid rootfstype=ext4 rootwait console=ttyS0,115200 stmmaceth=chain_mode:1


label alpineV12a
	menu label Alpine Linux visionfive V2-v1.2a
	kernel /vmlinuz-starfive
	initrd /initramfs-starfive
	fdt /dtbs-starfive/starfive/jh7110-starfive-visionfive-2-v1.2a.dtb
	append earlycon=sbi rw root=UUID=$rootuuid rootfstype=ext4 rootwait console=ttyS0,115200 stmmaceth=chain_mode:1

EOF

##
# Alpine rootfs.
##

# TODO: Alpine doesn't generic release images for RISC-V yet.
# For this reason, we are forced to create our own rootfs for now.
#
# This requires installing qemu-riscv64 and qemu-openrc as well as
# starting the qemu-binfmt OpenRC services. Otherwise, the post-install
# scripts cannot be executed.
apk --quiet --root "$MNTPNT" --arch riscv64 --initdb --update-cache --allow-untrusted \
	-X http://dl-cdn.alpinelinux.org/alpine/edge/main \
	-X http://dl-cdn.alpinelinux.org/alpine/edge/community \
	-X "$(pwd)/pkg/starfive" \
	add alpine-base linux-starfive linux-firmware-none ca-certificates u-boot-starfive \
		chrony tiny-cloud-alpine

echo "UUID=$bootuuid	/boot	ext4	defaults	1 1" >> "$MNTPNT"/etc/fstab

# Activate various OpenRC services by default
ln -s /etc/init.d/bootmisc "$MNTPNT"/etc/runlevels/boot/
ln -s /etc/init.d/hostname "$MNTPNT"/etc/runlevels/boot
ln -s /etc/init.d/modules "$MNTPNT"/etc/runlevels/boot
ln -s /etc/init.d/sysctl "$MNTPNT"/etc/runlevels/boot
ln -s /etc/init.d/urandom "$MNTPNT"/etc/runlevels/boot
ln -s /etc/init.d/devfs "$MNTPNT"/etc/runlevels/sysinit
ln -s /etc/init.d/swclock "$MNTPNT"/etc/runlevels/sysinit
ln -s /etc/init.d/hwdrivers "$MNTPNT"/etc/runlevels/sysinit
ln -s /etc/init.d/mdev "$MNTPNT"/etc/runlevels/sysinit
ln -s /etc/init.d/modules "$MNTPNT"/etc/runlevels/sysinit
ln -s /etc/init.d/mount-ro "$MNTPNT"/etc/runlevels/shutdown
ln -s /etc/init.d/killprocs "$MNTPNT"/etc/runlevels/shutdown

chroot "$MNTPNT" /sbin/tiny-cloud --enable

# Hack to enable autologin on ttyS0
printf '\n# Auto login on SiFive serial\n%s\n' \
	'ttyS0::respawn:/bin/login -f root' \
	>> "$MNTPNT"/etc/inittab

# See https://github.com/starfive-tech/linux/issues/54
# For some reason loading this module results in a kernel panic.
echo "blacklist snd_soc_starfive_i2svad" \
	>> "$MNTPNT"/etc/modprobe.d/blacklist.conf

# jh7110_crypto is problematic. disable for now
echo "blacklist jh7110_crypto" \
	>> "$MNTPNT"/etc/modprobe.d/blacklist.conf


# enable https repositories
printf "%s\n%s\n" \
	"https://dl-cdn.alpinelinux.org/alpine/edge/main" \
	"https://dl-cdn.alpinelinux.org/alpine/edge/community" \
	> "$MNTPNT"/etc/apk/repositories

df -h

# this requires the boot mode setting to be set to SDIO for booting from the SD-card
# https://doc-en.rvspace.org/VisionFive2/Boot_UG/VisionFive2_SDK_QSG/boot_mode_settings.html
dd if="$MNTPNT"/usr/share/u-boot/starfive_visionfive2/u-boot-spl.bin.normal.out of=${LOOP_DEVICE}p1
dd if="$MNTPNT"/usr/share/u-boot/starfive_visionfive2/u-boot.itb of=${LOOP_DEVICE}p2
