# Alpine Linux on the StarFive VisionFive

Scripts to create a SD card for booting [Alpine Linux][alpine web] Edge on the [StarFive VisionFive][rvspace] RISC-V SoC.

## Status

This is still in early stages of development, booting Alpine works but
there are still a few rough spots that need to be improved:

* The DesignWare MMC driver occasionally sometimes creates the
  device file for the SD card as `/dev/mmcblk0` and sometimes as
  `/dev/mmcblk1`. This causes mounting root to fail occasionally,
  instead mount root by `PARTUUID`.
* The image size should be made configurable
* Test more stuff
* Enable initrd

## Pre-built Images

Pre-built images can be downloaded [here](https://dev.alpinelinux.org/~nmeum/starfive-visionfive/).
These images only have to be flashed to the SD card using dd(1), see the next section for more information.

## Usage

The script is intended to be run on Alpine Linux Edge and thus uses
busybox utilities and requires a working [`apk(8)`][apk-tools web]
installation. Furthermore, a few packages need to be installed, e.g.
using the following command:

	# apk add sfdisk e2fsprogs qemu-riscv64 qemu-openrc lua-aports

The qemu packages are necessary to ease building a custom linux-kernel
[with the starfive patches][linux-starfive]. For this purpose,
`qemu-riscv64` needs to be registed with [`binfmt_misc`][linux binfmt],
e.g. using:

	# rc-service qemu-binfmt start

Afterwards, the `linux-starfive` package needs to be build using
`buildrepo` from the provided `APKBUILD` using:

	$ ./build-pkgs.sh

This will create a custom `linux-starfive` package in the `./pkg`
subdirectory. Using this Linux package, an SD card image can be built
with the following command:

	# ./build_image.sh

This image can then be flashed to the SD card using:

	# dd if=visionfive.img of=/dev/mmcblk0 bs=1M oflag=direct

Afterwards, put the SD card into the StarFive VisionFive, start it as
usual and a autologin prompt should appear on the serial console.

## License

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

[alpine web]: https://alpinelinux.org
[rvspace]: https://rvspace.org
[apk-tools web]: https://gitlab.alpinelinux.org/alpine/apk-tools
[linux binfmt]: https://www.kernel.org/doc/html/latest/admin-guide/binfmt-misc.html
[linux-starfive]: https://github.com/starfive-tech/linux
