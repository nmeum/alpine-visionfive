# Contributor: Milan P. Stanić <mps@arvanta.net>
# Maintainer: Milan P. Stanić <mps@arvanta.net>
pkgname=u-boot-jh7110
pkgver=0_git20230926
pkgrel=1
_commit=964aae164414b15ef91a0319decb13e7e2c06b52
pkgdesc="starfive JH7110 SoC u-boot"
url="https://www.denx.de/wiki/U-Boot/"
arch="riscv64"
license="GPL-2.0-or-later OFL-1.1 BSD-2-Clause BSD-3-Clause eCos-2.0 IBM-pibs
  ISC LGPL-2.0-only LGPL-2.1-only X11"
makedepends="
	bc
	bison
	dtc
	flex
	gnutls-dev
	linux-headers
	opensbi
	openssl-dev
	py3-elftools
	py3-setuptools
	python3-dev
	swig
	util-linux-dev
	"

source="https://source.denx.de/u-boot/u-boot/-/archive/$_commit/u-boot-$_commit.tar.gz
	0001-tools-mkimage-Add-StarFive-SPL-image-support.patch
	0001-riscv-dts-starfive-generate-u-boot-spl.bin.normal.ou.patch
	0001-configs-NVMe-USB-target-boot-devices-on-VisionFive-2.patch
	0001-starfive-visionfive2-add-mmc0-and-nvme-boot-targets.patch

	fix-boot-order.patch
	"
builddir="$srcdir/"u-boot-$_commit

build() {
	make starfive_visionfive2_defconfig
	make OPENSBI=/usr/share/opensbi/generic/firmware/fw_dynamic.bin
}

package() {
	mkdir -p "$pkgdir"/usr/share/$pkgname/
	install -m 0644 "$builddir"/spl/u-boot-spl.bin.normal.out "$pkgdir"/usr/share/$pkgname/u-boot-spl.bin.normal.out
	install -m 0644 "$builddir"/u-boot.itb "$pkgdir"/usr/share/$pkgname/u-boot.itb
}

sha512sums="
638673dde5012f7fe20949be4c004722da84c5d938ada7384c5848cae6a9a481035ea0722e81c7d9779e0f5e61c82085b1adf7b415a41628c5bfee26ac6e6313  u-boot-964aae164414b15ef91a0319decb13e7e2c06b52.tar.gz
b30a9117a270afe9a895bece2f1d2d79c1bbc95f78cd5c0eed6201884f20d4ce3cfbd303d0f82e1af4cf334c703589aa9bead6dd655ea39df46b067328b3a547  0001-tools-mkimage-Add-StarFive-SPL-image-support.patch
656f80a24ac793159e08b0796b981070c7ba45f5297b67e759ef66bd3eda812f1b7843107ef61f37ee0ec2f5d364c388692ffbfdf4581c9239eb24dff5004f13  0001-riscv-dts-starfive-generate-u-boot-spl.bin.normal.ou.patch
3a48443293f98c906793f7c67d068321b74b02c8e7e0f98fc46e5e63ba11420f4e4ef7f36f843b694b1d9be8c66e4df368f91dd444d034838e98d5949f6132c3  0001-configs-NVMe-USB-target-boot-devices-on-VisionFive-2.patch
2fc8060a0bb83ac6289f2200bee74c9bda3fd83b51014a5722044beab18c72318c48364602ce24f536c855a1de549e905fde815dbbd1c95907dde73ccaa2c1fc  0001-starfive-visionfive2-add-mmc0-and-nvme-boot-targets.patch
f23e967fca49fcc17e8165abcc29d672dd2af6c5164294993cd339a6566f81686c5252b3e995de34446df86d7f5c0881490d1076ec246e8b956510e61885256f  fix-boot-order.patch
"
